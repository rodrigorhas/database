ARG REDIS_VERSION

FROM redis:${REDIS_VERSION}

CMD [ "redis-server", "/usr/local/etc/redis/redis.conf" ]
